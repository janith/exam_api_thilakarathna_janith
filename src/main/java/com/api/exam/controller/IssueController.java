package com.api.exam.controller;

import com.api.exam.model.Issue;
import com.api.exam.model.IssueType;
import com.api.exam.model.State;
import com.api.exam.request.CreateIssueRequest;
import com.api.exam.service.IssueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/issue")
public class IssueController {

    @Autowired
    private IssueService issueService;

    @PostMapping
    public ResponseEntity<Issue> createIssue(@Valid @RequestBody CreateIssueRequest createIssueRequest) {
        return ResponseEntity.status(HttpStatus.CREATED).body(issueService.createIssue(createIssueRequest));
    }

    @GetMapping
    public ResponseEntity<List<Issue>> getAllIssues(@RequestParam(required = false) State currentState,
                                                    @RequestParam(required = false) IssueType type) {
        if (currentState == null && type == null) {
            return ResponseEntity.ok(issueService.getAllIssues());
        } else {
            return ResponseEntity.ok(issueService.getIssuesByCurrentStateAndType(currentState, type));
        }

    }

}
