package com.api.exam.controller;

import com.api.exam.exception.UserNotFoundException;
import com.api.exam.request.LoginRequest;
import com.api.exam.response.JwtResponse;
import com.api.exam.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/auth")
public class AuthController {

    @Autowired
    private AuthService authService;

    @PostMapping("/signin")
    public ResponseEntity<JwtResponse> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) throws UserNotFoundException {
        return ResponseEntity.ok(authService.authenticateUser(loginRequest));
    }

}
