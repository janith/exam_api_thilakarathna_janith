package com.api.exam.response;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@AllArgsConstructor
public class ErrorMessage {

    @NotNull
    private int statusCode;
    @NotNull
    private Date currentDateTime;
    @NotBlank
    private String message;
    @NotBlank
    private String description;

}
