package com.api.exam.response;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class JwtResponse {

    private final String type = "Bearer";
    private String token;
    private String refreshToken;
    private Long userId;
    private String userName;
    private String email;
}
