package com.api.exam.service;

import com.api.exam.model.Project;

import java.util.List;

public interface ProjectService {

    Project createProject(Project project);

    List<Project> getAllProjects();

}
