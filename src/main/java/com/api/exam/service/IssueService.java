package com.api.exam.service;

import com.api.exam.model.Issue;
import com.api.exam.model.IssueType;
import com.api.exam.model.State;
import com.api.exam.request.CreateIssueRequest;

import java.util.List;

public interface IssueService {

    Issue createIssue(CreateIssueRequest createIssueRequest);

    List<Issue> getAllIssues();

    List<Issue> getIssuesByCurrentStateAndType(State currentState, IssueType type);

}
