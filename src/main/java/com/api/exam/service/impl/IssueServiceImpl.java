package com.api.exam.service.impl;

import com.api.exam.model.Issue;
import com.api.exam.model.IssueType;
import com.api.exam.model.Project;
import com.api.exam.model.State;
import com.api.exam.repository.IssueRepository;
import com.api.exam.repository.ProjectRepository;
import com.api.exam.request.CreateIssueRequest;
import com.api.exam.service.IssueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public class IssueServiceImpl implements IssueService {

    @Autowired
    private IssueRepository issueRepository;

    @Autowired
    private ProjectRepository projectRepository;

    @Override
    public Issue createIssue(CreateIssueRequest createIssueRequest) {
        Issue newIssue = new Issue(createIssueRequest.getType(),
                createIssueRequest.getCurrentState());
        newIssue = issueRepository.save(newIssue);
        Project project = projectRepository.getById(createIssueRequest.getProjectId());
        Set<Issue> issues = project.getIssues();
        issues.add(newIssue);
        projectRepository.save(project);
        return newIssue;
    }

    @Override
    public List<Issue> getAllIssues() {
        return issueRepository.findAll();
    }

    @Override
    public List<Issue> getIssuesByCurrentStateAndType(State currentState, IssueType type) {
        return issueRepository.getIssuesByCurrentStateAndType(currentState, type);
    }
}
