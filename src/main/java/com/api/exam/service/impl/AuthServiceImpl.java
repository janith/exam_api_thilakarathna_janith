package com.api.exam.service.impl;

import com.api.exam.exception.TokenRefreshException;
import com.api.exam.exception.UserNotFoundException;
import com.api.exam.model.UserDetailsImpl;
import com.api.exam.repository.UserRepository;
import com.api.exam.request.LoginRequest;
import com.api.exam.response.JwtResponse;
import com.api.exam.service.AuthService;
import com.api.exam.util.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class AuthServiceImpl implements AuthService {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserRepository userRepository;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    JwtUtil jwtUtil;

    @Override
    public JwtResponse authenticateUser(LoginRequest loginRequest) throws UserNotFoundException {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getUserName(),
                        loginRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String accessToken = jwtUtil.generateJwt(authentication);

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        String refreshToken = jwtUtil.getRefreshToken(loginRequest.getUserName());

        return new JwtResponse(accessToken, refreshToken, userDetails.getId(), userDetails.getUsername(),
                userDetails.getEmail());
    }

    @Override
    public JwtResponse authenticateByRefreshToken(String refreshToken) {
        if (jwtUtil.validateJwt(refreshToken)) {
            String newAccessToken = jwtUtil.generateTokenFromUsername(jwtUtil.getUserNameFromJwt(refreshToken));
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
            String newRefreshToken = jwtUtil.getRefreshToken(userDetails.getUsername());
            return new JwtResponse(newAccessToken, newRefreshToken, userDetails.getId(), userDetails.getUsername(),
                    userDetails.getEmail());
        } else {
            throw new TokenRefreshException(refreshToken, "Invalid Token");
        }

    }
}
