package com.api.exam.service;

import com.api.exam.model.User;

public interface UserService {

    User createUser(User user);

}
