package com.api.exam.service;

import com.api.exam.exception.UserNotFoundException;
import com.api.exam.request.LoginRequest;
import com.api.exam.response.JwtResponse;

public interface AuthService {

    JwtResponse authenticateUser(LoginRequest loginRequest) throws UserNotFoundException;

    JwtResponse authenticateByRefreshToken(String refreshToken);

}
