package com.api.exam.repository;

import com.api.exam.model.Issue;
import com.api.exam.model.IssueType;
import com.api.exam.model.State;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IssueRepository extends JpaRepository<Issue, Long> {

    List<Issue> getIssuesByCurrentStateAndType(State currentState, IssueType type);

}
