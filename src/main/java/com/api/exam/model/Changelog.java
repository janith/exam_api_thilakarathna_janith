package com.api.exam.model;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "changelogs")
public class Changelog {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "changelog_id")
    private Long changelogId;
    @Column(name = "changed_on")
    private Date changedOn;
    @Column(name = "from_state")
    private State fromState;
    @Column(name = "to_state")
    private State toState;

}
