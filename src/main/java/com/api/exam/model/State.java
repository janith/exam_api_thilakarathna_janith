package com.api.exam.model;

public enum State {
    OPEN,
    IN_PROGRESS,
    TESTING,
    DEPLOY;
}
