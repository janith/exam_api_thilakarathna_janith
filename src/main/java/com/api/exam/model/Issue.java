package com.api.exam.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@Table(name = "issues")
@AllArgsConstructor
@NoArgsConstructor
public class Issue {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "issue_id")
    private Long issueId;
    @Column(name = "type")
    private IssueType type;
    @Column(name = "current_state")
    private State currentState;
    @OneToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "issue_changelogs",
            joinColumns = @JoinColumn(name = "issue_id"),
            inverseJoinColumns = @JoinColumn(name = "changelog_id"))
    private Set<Changelog> changelogs = new HashSet<>();
    public Issue(IssueType type, State currentState) {
        this.type = type;
        this.currentState = currentState;
    }

}
