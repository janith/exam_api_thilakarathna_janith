package com.api.exam.model;

public enum IssueType {
    BUG,
    STORY,
    TASK;
}
