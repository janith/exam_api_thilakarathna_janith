package com.api.exam.request;

import com.api.exam.model.IssueType;
import com.api.exam.model.State;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class CreateIssueRequest {

    @NotNull
    private Long projectId;
    @NotNull
    private IssueType type;
    @NotNull
    private State currentState;

}
