package com.api.exam.exception;

public class UserNotFoundException extends Exception {

    public UserNotFoundException(Long userId) {
        super(String.format("User not found for ID : [%s]", userId));
    }

}
