CREATE TABLE users
(
    user_id   INTEGER       NOT NULL,
    user_name VARCHAR2(20)  NOT NULL,
    email     VARCHAR2(50)  NOT NULL,
    password  VARCHAR2(120) NOT NULL,
    PRIMARY KEY (user_id)
);
CREATE TABLE projects
(
    project_id INTEGER      NOT NULL,
    name       VARCHAR2(20) NOT NULL,
    PRIMARY KEY (project_id)
);
CREATE TABLE changelogs
(
    changelog_id INTEGER      NOT NULL,
    changed_on   DATETIME     NOT NULL,
    from_state   VARCHAR2(20) NOT NULL,
    to_state     VARCHAR2(20) NOT NULL,
    PRIMARY KEY (changelog_id)
);
CREATE TABLE issues
(
    issue_id      INTEGER      NOT NULL,
    type          VARCHAR(20)  NOT NULL,
    current_state VARCHAR2(20) NOT NULL,
    PRIMARY KEY (issue_id)
);

CREATE TABLE issue_changelogs
(
    issue_id     INTEGER NOT NULL,
    changelog_id INTEGER NOT NULL
);
CREATE TABLE project_issues
(
    project_id INTEGER NOT NULL,
    issue_id   INTEGER NOT NULL
);